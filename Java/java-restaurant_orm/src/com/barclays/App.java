package com.barclays;

public class App {
    public static void main(String[] args) throws Exception {

        new DB("jdbc:sqlite:./restaurant_base.sqlite");

        //makes sure the table exists in the database. It it does it won't do anything, otherwise it will create it
        //will look for restaurants in the db. If there are we are going to make them into instances of a restaurant
        Restaurant.init();
        
        Restaurant restaurant = new Restaurant("Burger House", "http://image.bhouse.jpg");
        System.out.println(restaurant.getId());
        DB.conn.close();
    }
}
