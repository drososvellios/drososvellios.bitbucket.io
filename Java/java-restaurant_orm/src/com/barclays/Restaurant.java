package com.barclays;

import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Restaurant {
    private int id;
    private String name;
    private String imageURL;

    public static ArrayList<Restaurant> all = new ArrayList<>();

    public static void init() { 
        try  {
            Statement createTable = DB.conn.createStatement();
            createTable.execute("CREATE TABLE IF NOT EXISTS restaurants (id INTEGER PRIMARY KEY, name TEXT, imageURL TEXT);");

            // I want to iterate over existing records so I write to my ArrayList
            Statement getRestaurants = DB.conn.createStatement();
            ResultSet restaurants = getRestaurants.executeQuery("Select * from restaurants;");

            while (restaurants.next()) {
                //getting the values out of the rows
                int id = restaurants.getInt(1);
                String name = restaurants.getString(2);
                String imageURL = restaurants.getString(3);

                new Restaurant(id, name, imageURL);
            } 
        }   catch (SQLException error) {
        }   
    }

    public Restaurant(String name, String imageURL) {
        this.name = name;
        this.imageURL = imageURL;
        try {
            PreparedStatement insertRestaurant = DB.conn.prepareStatement("INSERT INTO restaurants (name, imageURL) VALUES (?, ?);");
            insertRestaurant.setString(1, this.name);
            insertRestaurant.setString(2, this.imageURL);
            insertRestaurant.executeUpdate();

            //insertRestaurant.getGeneratedKeys() returns the resultset - getInt(position) will return a particular one
            this.id = insertRestaurant.getGeneratedKeys().getInt(1);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        // will add a restaurant that has been created to the ArrayList
        Restaurant.all.add(this);
    }

    public Restaurant(int id, String name, String imageURL) {
        this.id = id; 
        this.name = name;
        this.imageURL = imageURL;
        Restaurant.all.add(this);
    }

    public int getId() { 
        return this.id;
    }


}
