public class Main { 

    public static void main(String[] args) {

        Hotel hotel = new Hotel("The Grand"); 
  
        Guest robinson = new Guest("Robinson");
        hotel.checkIn(robinson);

        Guest thanet = new Guest("Thanet");
        hotel.checkIn(thanet);

        System.out.println(hotel.getName());
       
    }
}  