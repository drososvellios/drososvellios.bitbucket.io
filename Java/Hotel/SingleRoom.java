public class SingleRoom extends Room {
    
    public SingleRoom (int number) {
        super(number);
        super.setPrice(79.00);
    }

    @Override
    public boolean isEmpty() {
        return this.beds[0] == null; 
    }

}
