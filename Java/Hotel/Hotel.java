public class Hotel {
    private String name;
    private Room[] rooms; 

    //I used the below way to define a room, by calling a constructor with name and number 
    //public Hotel(String name, int noOfRooms) {
    public Hotel(String name) {
        this.name = name;
        this.rooms = new Room[8];

        this.rooms[0] = new DoubleRoom(1); 
        this.rooms[1] = new SingleRoom(2); 
        this.rooms[2] = new SingleRoom(3); 
        this.rooms[3] = new SingleRoom(4); 
        this.rooms[4] = new DoubleRoom(5); 
        this.rooms[5] = new DoubleRoom(6);
        this.rooms[6] = new TwinRoom(7);
        this.rooms[7] = new TwinRoom(8);
       /*

       // I used the below way of filling in the rooms table, but as we introduced new 
       // ways of working, we are now constructing the individual rooms, above

        for(int i=0; i<this.rooms.length; i++) {
            this.rooms[i] = new Room(i +1); 
        }
        */
    }

    public String getName() {
        return this.name;
    }

    public void checkIn(Guest guest) {
        for (Room room: this.rooms) {
            if(room.isEmpty()) {
                room.addGuest(guest);
                break;
            }
        }
    }

}
