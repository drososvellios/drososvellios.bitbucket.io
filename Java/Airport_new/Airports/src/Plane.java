import java.util.Arrays;

public class Plane {
    private String flightNumber; 
    private Passenger[][] seats;

    public Plane(String flightNumber) {
        this.flightNumber = flightNumber;
        this.seats = new Passenger[][] {
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null}
        };
    }

    public void board(Passenger passenger) {
        int row = passenger.getRow();
        int seat = passenger.getSeat();
        this.seats[row][seat] = passenger;
    }

    public String getFLightNumber() {
        return this.flightNumber;
    }

    public Boolean hasPassenger(Passenger passenger) {
        Boolean hasPassenger = false; 
        for(Passenger[] row: this.seats) {
            hasPassenger = Arrays.asList(row).contains(passenger);
            if(hasPassenger) break;
        }
        return hasPassenger;
      
    }

}
