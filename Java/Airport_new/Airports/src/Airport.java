import java.util.HashMap;

public class Airport {
    private String code;
    private HashMap<String, Plane> planes;

    public Airport(String code) {
        this.code = code;
        this.planes = new HashMap<>();
    }

    public Plane getFlight(String flightNumber) {
        return this.planes.get(flightNumber);
    }

    public void departures(Passenger passenger) {

    }

    public void land(Plane plane) {
        this.planes.put(plane.getFLightNumber(), plane);
    }
}
