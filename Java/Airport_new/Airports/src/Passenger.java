public class Passenger {

    private Flight flight;
    private BoardingPass boardingPass;
    // private String name;

    public Passenger(Flight flight) {
        this.flight = flight;
        this.boardingPass = null; 
    }

    public String getOutbound(Passenger passenger) {
        return this.flight.outbound;
    }

    public String getInbound(Passenger passenger) {
        return this.flight.inbound;
    }

    public String getFlightNumber(Passenger passenger) {
        return this.flight.flightNumber;
    }

    public void setBoardingPass(BoardingPass boardingPass) {
        this.boardingPass = boardingPass;
    }

    public int getRow() {
        return this.boardingPass== null ? null: this.boardingPass.row;
    }

    public int getSeat() {
        return this.boardingPass== null ? null: this.boardingPass.seat;

    }

}
