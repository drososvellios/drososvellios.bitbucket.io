public class Flight {
   
    //try setting these to be private and get hold of their values through geters and seters 
    public String outbound;
    public String inbound;
    public String flightNumber;

    public Flight(String outbound, String inbound, String flightNumber) {
        this.outbound = outbound;
        this.inbound = inbound;
        this.flightNumber = flightNumber;
    }
    
}
