import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class AirportTests {

    @Test
    public void an_airport_has_planes() {
        Airport LHR = new Airport("LHR");
        Plane plane = new Plane("LA992");
        LHR.land(plane);
        assertEquals(LHR.getFlight("LA992"), plane);
    }

    @Test
    public void after_departures_passenger_has_boarding_pass() {
        Airport LHR = new Airport("LHR");
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        BoardingPass bPass = new BoardingPass(1,2);
        passenger.setBoardingPass(bPass);
        LHR.departures(passenger);
        assertEquals(passenger.getRow(), 1);
    }

    @Test
    public void an_airport_has_departures() {
        // Create an airport
        Airport LHR = new Airport("LHR");
        // Create a passenger
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        // The passenger arrives at departures and get boarded on the plane (so
        // "departures has to be a function of the Airport class")
        LHR.departures(passenger);
        // I get hold of the plane through the airport ('getFlight has to be a function
        // of the class Airport')
        Plane plane = LHR.getFlight("LA992");
        // I assert the below boolean statement ('has Passenger has to be a function of
        // the class Plane')
        assertTrue(plane.hasPassenger(passenger));
    }
}
