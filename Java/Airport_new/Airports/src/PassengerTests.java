import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;

public class PassengerTests {
    
    @Test
    public void aPersonHasAFlight() { 
        Passenger passenger = new Passenger(new Flight("LHR","LAX", "LA299"));
        assertEquals(passenger.getOutbound(passenger), "LHR");
    }

    @Test
    public void has_a_boarding_pass() {
        Passenger passenger = new Passenger(new Flight("LHR","LAX", "LA299"));
        BoardingPass bPass = new BoardingPass(2, 3);
        passenger.setBoardingPass(bPass);
        assertEquals(passenger.getRow(), 2);
        assertEquals(passenger.getSeat(), 3);
        };
        
    }

