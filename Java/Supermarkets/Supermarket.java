import java.util.ArrayList;
import java.util.HashMap;
import java.util.ArrayDeque;

public class Supermarket {
   //The List data structure (implementation of the Collection interface) allow us to create an array, but without the limitation of having to declare upfront how many items will be in the array. 
   //Below I am declaring an Arraylist (deriving from List) of the type Product, called inventory
    private ArrayList<Product> inventory = new ArrayList<>(); 

    //A Map stores things in key-values pairs. The idea of this data structure is we can look things up really quickly and easily.
    //The HashMap is an implementation of the Map interface
    private HashMap<Integer, Integer> stock = new HashMap<>();

    //The ArrayDeque class implements these two interfaces:
    //Queue Interface: It is an Interface that is a FirstIn – FirstOut Data Structure where the elements are added from the back. (We are using it as this one here)
    //Deque Interface: It is a Doubly Ended Queue in which you can insert the elements from both sides. It is an interface that implements the Queue.
    private ArrayDeque<Shopper> checkout = new ArrayDeque<>(); 

    public Supermarket(){ 

        //CREATE: add() is a method that exists in List
        inventory.add(new Product("Coffee", 2.79));
        inventory.add(new Product("Tea", 1.79));
        inventory.add(new Product("Biscuits", 3.20));
        inventory.add(new Product("Milk", 0.82));
        inventory.add(new Product("Oat Milk Milk", 3.20));


        /*

        //READ: get() is a method that exists in List. It takes an index as argument, like an array (0-indexed)
        Product coffee = inventory.get(0);

        //UPDATE: set() is List method that gets used to update values. It expects and index and a "Product" element
        inventory.set(0, new Product("Coffee", 2.99));

        //DELETE: remove gets called with an index 
        inventory.remove(3);

        //testing how the my inventory has been updated after the CRUD function above
        inventory.size();
        inventory.get(0).getPrice();
        System.out.println("");

        */

        for(Product product : inventory) {

            // product.getBarcode() is going to be the key, and I have decided to start with a value of 10 for every item.
            // The key is going to be the barcode and the value is going to be the stock level
            stock.put(product.getBarcode(), 10);
        }

        Shopper shopper1 = new Shopper();
        Shopper shopper2 = new Shopper();

        shopper1.addProduct(inventory.get(0));
        shopper1.addProduct(inventory.get(0));
        shopper2.addProduct(inventory.get(1));
        shopper2.addProduct(inventory.get(2));
        shopper2.addProduct(inventory.get(3));

        checkout.addLast(shopper1);
        checkout.addLast(shopper2);

        processQueue();

        System.out.println("");

    }
    public static void main(String[] args) {

        new Supermarket();
    }

    private void processQueue() {
        while(!checkout.isEmpty()) {
            double total = 0;
            //pollFirst() will remove an item (the shopper) from the queue and return us the shopper
            Shopper shopper = checkout.pollFirst();

            while(shopper.hasProducts()) { 
                Product product = shopper.getProduct();
                int barcode= product.getBarcode();
                total += product.getPrice();
                int currentStockValue = stock.get(barcode);
                stock.replace(barcode, currentStockValue - 1);
            }
            System.out.printf("Shopper %s total £%f\n", shopper.toString(), total);

        }
    }


} 