import java.util.Stack;
public class Shopper {

    //In the Stack structure items are put on the stack then taken off it, in order. It is like putting things in your shopping trolly, the first items you put in are the last ones to come out.
    private Stack<Product> trolly = new Stack<>(); 

    public void addProduct(Product product) { 
        //push() adds items to the stack
        trolly.push(product); 
    }

    public Product getProduct() {
        //pop() removes the top item from the stack
        return trolly.pop(); 
    }

    public boolean hasProducts() {
        //here, the isEmpty() method is built-in the Stack interface
        //It tests if this vector has no components.
        return !trolly.isEmpty();
    }
    
}
