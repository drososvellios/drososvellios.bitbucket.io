package com.barclays.httpserver;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//in this class our endpoints will be defined and created
//Part of the Controller's job is to deal with the http requests, so it needs to have access to the restaurants

@RestController
public class RestaurantController {
    private RestaurantRepository repository;

    //when the Controller gets instantiated, all of the repositories will get passed into the constructor 
    public RestaurantController(RestaurantRepository repository) {
        this.repository = repository;
    }

    //The first thing to do is to read all restaurants 
    //the anotations includes the RESTful route "/restaurants"
    @GetMapping("/restaurants")
    //method of the type List<Restaurant>
    public List<Restaurant> getRestaurants(){
        return this.repository.findAll();
    }

    //creating restaurants
    //the usual practice is when you create a resourse, to have the thing you created returned back to you
    @PostMapping("/restaurants")
    //this will send a name and imageURL to the RequestBody and the id will also be generated automatically
    //The "RequestBody" will read the submitted values from the json file
    public Restaurant createRestaurant(@RequestBody Restaurant restaurantData) {
        //we are saving the restaurant that has been sent to us
        //by saving, we will create an id and an instance of the restaurant object, and then send that back to the client
        return repository.save(restaurantData);
    }

    //reading one restaurant
    @GetMapping("/restaurants/{id}") 
    //@PathVariable annotation can be used to handle template variables in the request URI mapping, and set them as method parameters.@PathVariable annotation can be used to handle template variables in the request URI mapping, and set them as method parameters
    //We use the @PathVariable annotation to extract the templated part of the URI, represented by the variable {id}.
    public Restaurant getOne(@PathVariable Integer id) {
    //the next expression will return an "optional object". It will return a restaurant if it exists
    return repository.findById(id).get();
    }

    //this is going to http requests that are made with the 'put' method
    @PutMapping("/restaurants/{id}") 
    public Restaurant updateOne(@PathVariable Integer id, @RequestBody Restaurant restaurantUpdate) {
        //Below, "map" will map over the repository, or will throw an error if there is nothing inside it
        //1.find the restaurant by id
        //2.update all fields
        //3.save and show the updated values
        return repository.findById(id).map(restaurant ->  {
            restaurant.setName(restaurantUpdate.getName());
            restaurant.setImageURL(restaurantUpdate.getImageURL());
            return repository.save(restaurant);
        }).orElseThrow();
    }

    @DeleteMapping("/restaurants/{id}")
    public void deleteOne(@PathVariable Integer id) {
        repository.deleteById(id);
    }

}
