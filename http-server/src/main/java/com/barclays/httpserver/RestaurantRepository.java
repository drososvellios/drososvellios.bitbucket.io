package com.barclays.httpserver;

import org.springframework.data.jpa.repository.JpaRepository;

//This is a way of representing all of our restaurants and having an interface (or way to access) a specific restaurant
interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {}
