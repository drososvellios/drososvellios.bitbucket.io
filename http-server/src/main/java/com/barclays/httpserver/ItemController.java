package com.barclays.httpserver;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {
    private ItemRepository IRepo; 
    private MenuRepository MRepo;
    private RestaurantRepository RRepo;

    public ItemController(ItemRepository IRepo, MenuRepository MRepo, RestaurantRepository RRepo) {
        this.IRepo = IRepo;
        this.MRepo = MRepo;
        this.RRepo = RRepo;
    }

    @PostMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items")
    public Item addItem(@RequestBody Item ItemData, @PathVariable Integer menu_id) {
        //Restaurant restaurant = RRepo.findById(restaurant_id).get();
        Menu menu = MRepo.findById(menu_id).get();
        ItemData.setMenu(menu);
        return IRepo.save(ItemData); 
        
    }

    
}

