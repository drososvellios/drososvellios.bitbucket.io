package com.barclays.httpserver;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

//The below anotations will take the class definition and extend it by writting code on top of it
//The "Entity" anotation will create the equivalent of an ORM, abstracting away the persistense of our data model
@Entity
public class Restaurant {
    //The "Id" anotation will set the id for the restaurants and the "GeneratedValue" will auto-generate and increment the id's  
    //Similar to what we previously did with creating an ORM without the user having to create it, the java persistence will take care of the id's here    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String imageURL;

    //this class has many menus
    //fetching all the menus for each restaurant. EAGER/LAZY refer to when the data gets loaded into a memory. 
    //EAGER loading will get all the menus for one restaurant straight off and return them 
    //LAZY loading is a design pattern that we use to defer initialization of an object as long as it's possible  
    //the joining column "restaurant_id" will be generated automatically by the Entity framework
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "restaurant_id")
    private List<Menu> menus;
    
    public Restaurant() {}

    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getImageURL() {
        return this.imageURL;
    }
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    public List<Menu> getMenus() {
        return this.menus;
    }

}
