let data = []
const state = {menuOpen: null}
var register_state = false; 
var start_index = 0;
var end_index = 5;

fetch('/restaurants') //fetch is a Promise-based asynchronous request
.then(response => response.json()) //json is a method of the Response class. Calling json on the response of the server in order to turn the server response (string) into a js object
.then(_data => {  // _data represents the response from the server
    data = _data 
    render();
    //renderImg(slice[0].imageURL, "center-image", "centerImageId");
}) 
.catch(err => console.error(err))  //in case there is any error

function render() {
    slice = data.slice(start_index,end_index);

    const content = slice.map((restData,i) => { //The map() method creates a new array populated with the results of calling a provided function on every element in the calling array.
        return `
            <article class="rest-card">
                <div style="background-image: url('${restData.imageURL}');"></div>
                <footer>
                    <h2>${restData.name}</h2>
                    <button onclick="displayMenus(${i})">Open</button>
                <footer>
            </article>
        `
    }).join("")

    const appEl = document.getElementById('app') 
    appEl.innerHTML = content

    if (state.menuOpen) {
        
    const menuContent = `
        <section class = "menu-items">
            <article>
                ${state.menuOpen.map(menu => {
                    return `<h3>${menu.title}</h3>`
                }).join("")}
            </article>
        </section>
        `
    const menuEl = document.getElementById('menus')
    menuEl.innerHTML = menuContent
    }

    if (register_state) {
    RegisterNewForm();
    } else {
        const modalRegEl = document.getElementById('modal-register')
        modalRegEl.innerHTML = ""
    }
}

function displayMenus(index) {

    const ImageOpen = slice[index].imageURL;
    const restImage = `<img id="center-image" alt="center-image" src=${ImageOpen}>`
    const ImgEl = document.getElementById("center-image")
    ImgEl.innerHTML = restImage;   

    state.menuOpen = slice[index].menus
    render();
}

function closeModal() {
    state.menuOpen = null;
    render();
}

function RegisterNewForm () {
    const modal_register =  `
    <section class = "modal-register-bg">
    <article> 
    <form onsubmit="event.preventDefault(); addRestaurant(this);">
    <h2> Registration Form </h2>
    <label> Restaurant Name </label>
    <input name = "name" required/>
    <label id="imgLabel"> Image URL </label>
    <input name = "imageURL" type="url" required/>
    <button> Submit </button>
    <button onclick = "closeRegister()"> Close </button>
    </form>
    </article>    
    `   
    const modalRegEl = document.getElementById('modal-register')
    modalRegEl.innerHTML = modal_register
}

function addRestaurant(HTMLform) { 
    const data = new FormData(HTMLform) // we are using the FormData constructor to turn the HTML form into a data object in our js code so we can extract the values from it
    const  name = data.get('name');
    const imageURL = data.get('imageURL');
    
    fetch('/restaurants', {
        method: 'POST',
        headers: {
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({name, imageURL})
    }).then(res => res.json())
    .then(rest => {
        data.push(rest) 
        render()
    })
    .catch(console.error)  //using fetch to post the data to the server
}

function openRegister() {
    register_state = true;
    render();
}

function closeRegister() {
    register_state = false; 
    render();
}

function goRight() {
    if (start_index + 5 < data.length) {
        start_index = start_index + 1;
        end_index = end_index + 1;
    render();
    }
}

function goLeft() {
    if (start_index > 0) {
        start_index = start_index - 1;
        end_index = end_index -1;
    render();
    }
}
