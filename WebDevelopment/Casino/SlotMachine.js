//holds the balance and gets updated after each try
var balance = 0;
//can be set to 5 or 10 before the start of the game
var stake = 0;
//counter that gets used in the spinWheel function 
//once all the wheels have spinned (spins == 0), the clearInterval will run 3 times (to stop each reel separately) based on this counter. reelsSpinned will then reset to 0, to allow for another play
var reelsSpinned = 0;

const fruitReel = new Array();

fruitReel[0] = "<img src=\'01.png'>"

fruitReel[1] = "<img src=\'02.png'>"

fruitReel[2] = "<img src=\'03.png'>"

fruitReel[3] = "<img src=\'04.png'>"

fruitReel[4] = "<img src=\'05.png'>"

fruitReel[5] = "<img src=\'06.png'>"

//creates 3 copies of the fruitReel array
const reel1 = fruitReel.slice(0)
const reel2 = fruitReel.slice(0)
const reel3 = fruitReel.slice(0)

// assigns a score to the elements of the array
const map1 = new Map();

map1.set(fruitReel[0], 50)
map1.set(fruitReel[1], 10)
map1.set(fruitReel[2], 25)
map1.set(fruitReel[3], 15)
map1.set(fruitReel[4], 5)
map1.set(fruitReel[5], 20)

//gets called inside the spinWheel function 
function score() {

    let score = 0;

    if (reel1[2] == reel2[2] && reel1[2] !== reel3[2]) {

        score = map1.get(reel1[2]) + map1.get(reel2[2])

    } else if (reel1[2] !== reel2[2] && reel1[2] == reel3[2]) {

        score = map1.get(reel1[2]) + map1.get(reel3[2])

    } else if (reel1[2] !== reel2[2] && reel2[2] == reel3[2]) {

        score = map1.get(reel2[2]) + map1.get(reel3[2])

    } else if (reel1[2] == reel2[2] && reel1[2] == reel3[2]) {

        score = map1.get(reel1[2]) + map1.get(reel2[2]) + map1.get(reel3[2])
    } else {
        score = 0
    }
    msg4 = "Your score is " + score + " You win £" + score / 10
    renderResult(msg4)

    balance += score / 10 - stake

    msg2 = 'Balance £' + balance
    renderBalance(msg2)

}

//targets the "add - credit" element in the HTML footer and populates it with the a message
//the msg takes it value in the setStake function (dependecy on setCredit) which gets called every time the user runs out of credit
function addCredit(msg1) {
    const addCreditElement = document.getElementById("add-credit")
    addCreditElement.innerHTML = msg1
    addCreditElement.classList.remove('credit')
    msg1 === "Please add CREDIT, make a BET and SPIN" ? addCreditElement.classList.add('credit') : null
}

function renderBalance(msg2) {
    const newBalanceElement = document.getElementById("new-balance-message")
    newBalanceElement.classList.remove('balance')
    newBalanceElement.innerHTML = msg2
    newBalanceElement.classList.add('balance')
}

//targets the "new-stake-message" in the HTML footer and populates it with the a message
//the msg takes it value in the setStake function (dependecy on setCredit) which gets called every time the user runs out of credit
function renderStake(msg3) {
    const newStakeElement = document.getElementById("new-stake-message")
    newStakeElement.innerHTML = msg3
}

//targets the "result-message" in the HTML footer and populates it with the a message
//the msg takes it value in the score() function which gets called every time the spinWheel() function runs
function renderResult(msg4) {
    const newResultElement = document.getElementById("result-message")
    newResultElement.innerHTML = msg4
}

//targets a particular img in HTML and fills it in with an element of an array(reelIndex) 
function setImage(reelIndex, img) {
    const newImageElement = document.getElementById(img)
    newImageElement.innerHTML = reelIndex
}

//iterates over the 3 reels (i) and each element (j) of each reel and sets the initial image
//gets run upon initial load
function setStartingImages() {

    for (let i = 1; i < 4; i++) {
      
        for (let j = 0; j <= 4; j++) {
            setImage(fruitReel[j], "img" + (i + 3 * j))
        }
    }   
}

//on click function. Gets called from <main> html with input = 10 or 20
function setCredit(input) {
    msg2 = ''
    input = parseInt(input)

    if (balance + parseInt(input) >= 10) {
        balance += input
        msg2 = 'Balance £' + balance
        renderBalance(msg2)
        screenUpdate()

        setStake()

    } else {
        balance += input
        msg2 = 'Balance £' + balance
        renderBalance(msg2)
    }
}

//on click function. Gets called fron <main> html with input = 5 or 10
function setStake(input) {
    msg3 = ''

    if (balance + parseInt(input) >= 10) {
        input = parseInt(input)
        stake = input
        msg3 = 'Stake £' + stake
        renderStake(msg3)

        screenUpdate()
    }  
}

//runs upon page load and from inside other functions every time the screen message needs to be updated
function screenUpdate() {

    if (balance < stake || balance < 5) {
        msg1 = "Please add CREDIT, make a BET and SPIN"
        addCredit(msg1)
    } else if (stake == 0) {
        msg1 = "Make a BET and SPIN"
        addCredit(msg1)
    } else {
        msg1 = "Ready to SPIN?"
        addCredit(msg1)
    }
}

//onclick function that spins each wheel separately with a different delay and so each reel can get a different number of spins
function makePlay() {

    screenUpdate()

    if ((balance >= stake) && (balance > 0) && (stake > 0)) {

        spinWheel(reel1, 1, 350)
        spinWheel(reel2, 2, 450)
        spinWheel(reel3, 3, 200)

    }
}

//the function gets called with a reel that will undergo a pop&unshift on each itteration of the setInterval
//clearInterval will stop the setInterval which would otherwise run perpetually
//reelsSpinned gets used as a counter, so the score only updates when it reaches the value of 3
function spinWheel(reel, reelNo, delay) {

    let spins = Math.ceil(Math.random() * 10)*2;
    let interval = 0;


    interval = setInterval(function () {

        if (spins == 0) {
            clearInterval(interval);
            reelsSpinned++
            if (reelsSpinned == 3) {
                score()
                reelsSpinned = 0;
            }
        } else {
            reel.unshift(reel.pop())
            for (let j = 0; j <= 4; j++) {
                setImage(reel[j], "img" + (reelNo + 3 * j))
            }
            spins--
        }
    }, delay)
}

setStartingImages()
screenUpdate()




